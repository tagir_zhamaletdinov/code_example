<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;


class ReportsComponent extends CBitrixComponentgi
{
    public function onPrepareComponentParams($arParams)
    {
        if(!isset($arParams["CACHE_TIME"]))
            $arParams["CACHE_TIME"] = 36000000;

        $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
        if($arParams["IBLOCK_TYPE"] == '')
            $arParams["IBLOCK_TYPE"] = "news";
        $arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
        $arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]!="N";

        $arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
        if($arParams["SORT_BY1"] == '')
            $arParams["SORT_BY1"] = "ACTIVE_FROM";
        if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
            $arParams["SORT_ORDER1"]="DESC";

        if($arParams["SORT_BY2"] == '')
        {
            if (mb_strtoupper($arParams["SORT_BY1"]) == 'SORT')
            {
                $arParams["SORT_BY2"] = "ID";
                $arParams["SORT_ORDER2"] = "DESC";
            }
            else
            {
                $arParams["SORT_BY2"] = "SORT";
            }
        }
        if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
            $arParams["SORT_ORDER2"]="ASC";
        
        return $arParams;
    }

    public function executeComponent()
    {
        global $USER;

        $result_cache_key = __CLASS__ . md5(serialize([
                $USER->GetGroups(),
            ]));

        if ($this->startResultCache(false, $result_cache_key))
        {
            if(!Loader::includeModule("iblock"))
            {
                $this->abortResultCache();
                ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
                return false;
            }

            $this->arResult['SECTIONS'] = $this->getSections();

            $this->arResult['ITEMS'] = $this->getItems();

            if(empty($this->arResult['ITEMS']))
            {
                $this->abortResultCache();
                return false;
            }

            $this->includeComponentTemplate();
        }
    }

    /***
     * Получаем список отчетов и группируем
     *
     * @return array
     */
    protected function getItems(): array
    {
        $arResult = [];

        $el_res = ElementTable::getList([
            'order' => array($this->arParams['SORT_BY1'] => $this->arParams['SORT_ORDER1'], $this->arParams['SORT_BY2'] => $this->arParams['SORT_ORDER2']),
            'select' => [
                'ID',
                'NAME',
                'DETAIL_PICTURE',
                'IBLOCK_SECTION_ID',

                'PROP_ID' => 'PROPERTY.ID',
                'PROP_NAME' => 'PROPERTY.NAME',
                'PROP_CODE' => 'PROPERTY.CODE',
                'PROP_MULTIPLE' => 'PROPERTY.MULTIPLE',
                'PROP_VALUE' => 'ELEMENT_PROPERTY.VALUE',
                'PROP_ENUM_VALUE' => 'PROPERTY_ENUMERATION.VALUE',
            ],
            'filter' => ['=ACTIVE' => 'Y', 'IBLOCK_ID' => $this->arParams['IBLOCK_ID']],
            'runtime' => [
                new Reference(
                    'ELEMENT_PROPERTY',
                    ElementPropertyTable::class,
                    Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID'),
                ),
                new Reference(
                    'PROPERTY',
                    PropertyTable::class,
                    Join::on('this.ELEMENT_PROPERTY.IBLOCK_PROPERTY_ID', 'ref.ID')
                ),
                new Reference(
                    'PROPERTY_ENUMERATION',
                    PropertyEnumerationTable::class,
                    Join::on('this.ELEMENT_PROPERTY.VALUE', 'ref.ID')
                ),
            ],
        ]);
        while($ob = $el_res->fetch())
        {
            if($ob['PROP_MULTIPLE'] == 'Y') {
                $arProp[$ob['ID']][$ob['PROP_ID']][] = $ob['PROP_ENUM_VALUE']?:$ob['PROP_VALUE'];
            } else {
                $arProp[$ob['ID']][$ob['PROP_ID']] = $ob['PROP_ENUM_VALUE']?:$ob['PROP_VALUE'];
            }

            $props[$ob['ID']][$ob['PROP_CODE']] = [
                'ID' => $ob['PROP_ID'],
                'NAME' => $ob['PROP_NAME'],
                'CODE' => $ob['PROP_CODE'],
                'VALUE' => $arProp[$ob['ID']][$ob['PROP_ID']],
            ];

            $arResult['TEMP'][$ob['ID']] = [
                'ID' => $ob['ID'],
                'NAME' => $ob['NAME'],
                'IBLOCK_SECTION_ID' => $ob['IBLOCK_SECTION_ID'],
                'DETAIL_PICTURE' => CFile::GetPath($ob['DETAIL_PICTURE']),
                'PROPERTIES' => $props[$ob['ID']]
            ];
        }

        foreach ($arResult['TEMP'] as $arItem) {
            $reportName = $arItem['PROPERTIES'][$this->arParams['PROPERTY_REPORT_CODE']]['VALUE'];
            $sectionName = $this->arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['NAME'];

            $arResult[$reportName][$sectionName][] = $arItem;
        }

        unset($arResult['TEMP']);

        return $arResult;
    }

    protected function getSections(): array
    {
        $arResult = [];

        $el_res = SectionTable::getList([
            'select' => [
                'ID',
                'NAME',
            ],
            'filter' => ['=ACTIVE' => 'Y', 'IBLOCK_ID' => $this->arParams['IBLOCK_ID']],
        ]);
        while($ob = $el_res->fetch())
        {
            $arResult[$ob['ID']] = [
                'ID' => $ob['ID'],
                'NAME' => $ob['NAME']
            ];
        }

        return $arResult;
    }
}