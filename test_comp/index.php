<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>

<?
$APPLICATION->IncludeComponent(
	"custom:report.list", 
	".default", 
	array(
		"IBLOCK_ID" => "59",
		"IBLOCK_TYPE" => "info",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"PROPERTY_REPORT_CODE" => "REPORT_TYPE",
		"COMPONENT_TEMPLATE" => ".default",
		"SORT_BY1" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
	),
	false
);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>