<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
/** @var array $arResult */

?>


<section>
    <div class="report__type-wrapper">
        <?$count = 1;
        foreach ($arResult['ITEMS'] as $typeName => $arTypeItems) {?>
            <div class="report__type-title" data-target="<?=$count?>"><?=$typeName?></div>
        <?$count++;
        }?>
    </div>
    <?
    $count = 1;
    foreach ($arResult['ITEMS'] as $typeName => $arTypeItems) {?>
        <div class="report__type-section <?=$count == 1?'active':''?>" data-type="<?=$count?>">
            <?foreach ($arTypeItems as $sectionName => $arSectItems) {?>
                <div class="report__section-title"><?=$sectionName?></div>
                <div class="report__section-list">
                    <?foreach ($arSectItems as $arItem) {?>
                        <?if(!empty($arItem['PROPERTIES']['REPORT_FILE']['VALUE'])) {?>
                            <a target="_blank" href="<?=CFile::GetPath($arItem['PROPERTIES']['REPORT_FILE']['VALUE'])?>">
                                <div class="report-item__title"><?=$arItem['NAME']?></div>
                            </a>
                        <?} else {?>
                            <div class="report-item__title"><?=$arItem['NAME']?></div>
                        <?}?>
                    <?}?>
                </div>
            <?}?>
        </div>
    <?$count++;
    }
    ?>
</section>