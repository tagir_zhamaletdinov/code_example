document.addEventListener('DOMContentLoaded', function () {

    let chooseOneBtns = document.querySelectorAll(
        '.report__type-title[data-target]'
    )

    let reportsTabs = document.querySelectorAll(
        '.report__type-section[data-type]'
    )

    for (let btn of chooseOneBtns) {
        btn.addEventListener('click', function () {
            let target = btn.dataset.target;

            for (let tab of reportsTabs) {
                let tabType = tab.dataset.type;

                if(tabType == target) {
                    tab.classList.add('active');
                } else {
                    tab.classList.remove('active');
                }
            }
        })
    }
})